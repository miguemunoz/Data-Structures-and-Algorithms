#include <iostream>
#include <vector>

using std::vector;

/*
To summarize, initially we have k = n and l = 1. To solve a (k, l)-subproblem, we do the following. If
k ≤ 2l, we output just one summand k. Otherwise we output l and then solve the subproblem (k − l, l + 1).
*/

vector<int> optimal_summands(int n) {
  vector<int> summands;

	int l = 1;

	while (n > 0)
	{
	    if (n <= (2*l))
		{
			summands.push_back(n);
			break;
		}
		else
		{
		
			summands.push_back(l);
			n -= l;    
		}
			l++;

	}
	
  return summands;
}

int main() {
  int n;
  std::cin >> n;
  vector<int> summands = optimal_summands(n);
  std::cout << summands.size() << '\n';
  for (size_t i = 0; i < summands.size(); ++i) {
    std::cout << summands[i] << ' ';
  }
}
