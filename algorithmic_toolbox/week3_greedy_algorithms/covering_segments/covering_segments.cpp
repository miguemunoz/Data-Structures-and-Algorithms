#include <algorithm>
#include <iostream>
#include <climits>
#include <vector>

using std::vector;

struct Segment {
  int start, end;
};

bool comp (Segment i, Segment j) {
	return (i.end < j.end);
}


vector<int> optimal_points(vector<Segment> &segments) {
  vector<int> points;

  std::sort(segments.begin(), segments.end(), comp);	

  int rght_most = segments[0].end;  
//  points.push_back(segments[0].start);
  points.push_back(segments[0].end);
		 
  for (size_t i = 1; i < segments.size(); ++i) {

    if (rght_most < segments[i].start)
	{
//    	points.push_back(segments[i].start);
   		points.push_back(segments[i].end);
		rght_most = segments[i].end;
    }

  }



  return points;
}

int main() {
  int n;
  std::cin >> n;
  vector<Segment> segments(n);
  for (size_t i = 0; i < segments.size(); ++i) {
    std::cin >> segments[i].start >> segments[i].end;
  }
  vector<int> points = optimal_points(segments);
  std::cout << points.size() << "\n";
  for (size_t i = 0; i < points.size(); ++i) {
    std::cout << points[i] << " ";
  }
}
