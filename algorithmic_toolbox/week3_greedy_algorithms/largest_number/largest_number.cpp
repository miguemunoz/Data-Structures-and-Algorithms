#include <algorithm>
#include <sstream>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

bool comp(string a, string b)
{
//	cout << stoi(a+b) << endl;
//	cout << stoi(b+a) << endl;
	return (stoi(a+b) > stoi(b+a));
}

string largest_number(vector<string> a) {

  std::sort (a.begin(), a.end(), comp);

  std::stringstream ret;
  for (size_t i = 0; i < a.size(); i++) {
    ret << a[i];
  }
  string result;
  ret >> result;
  return result;
}

int main() {
  int n;
  std::cin >> n;
  vector<string> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    std::cin >> a[i];
  }
  std::cout << largest_number(a);
}
