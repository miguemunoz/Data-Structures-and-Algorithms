#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;

struct Item {
	int value;
	int weight;
	double ratio;
};


bool comp (Item i, Item j) {
return (i.ratio>j.ratio); }



double get_optimal_value(int capacity, vector<int> weights, vector<int> values, vector<Item>& elems) {
  double value = 0.0;
  

  std::sort(elems.begin(), elems.end(), comp);

  for (std::vector<Item>::iterator it = elems.begin(); it != elems.end(); ++it)
  {
  	//std::cout << (*it).ratio << std::endl;  

	if ((*it).weight <= capacity)
	{
		capacity -= (*it).weight;
		value += (*it).value;
	}
	else
	{
		value += (*it).ratio * capacity;
		capacity = 0;
		break;
	}	
  }

  return value;
}

int main() {
  int n;
  int capacity;
  std::cin >> n >> capacity;
  vector<int> values(n);
  vector<int> weights(n);
  vector<Item> elems(n);
  for (int i = 0; i < n; i++) {
    std::cin >> values[i] >> weights[i];
    elems[i].value = values[i];
	elems[i].weight = weights[i];
	elems[i].ratio = (double)values[i] / weights[i];
  }

  double optimal_value = get_optimal_value(capacity, weights, values, elems);

  std::cout.precision(10);
  std::cout << optimal_value << std::endl;
  return 0;
}
