def maxPairwiseProductFast(a):
    x = max(a)
    a.remove(x)
    y = max(a)
    return x*y

def maxPairwiseProduct(a):
    result = 0
    n = len(a)
    for i in range(0, n):
        for j in range(i+1, n):
            if (a[i]*a[j] > result):
                result = a[i]*a[j]
    return result

n = int(input())
a = [int(x) for x in input().split()]
assert(len(a) == n)

print(maxPairwiseProductFast(a))
