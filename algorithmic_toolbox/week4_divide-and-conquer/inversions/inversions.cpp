
#include <iostream>
#include <vector>

using std::vector;

unsigned long long merge(vector<int> &a, vector<int> &b, size_t left, size_t mid, size_t right) {
   
  unsigned long long int i, j, k;
  unsigned long long inv_count = 0;

//    std::cout << "ping" << std::endl; 
  i = left; /* i is index for left subarray*/
  j = mid;  /* j is index for right subarray*/
  k = left; /* k is index for resultant merged subarray*/

  while ((i < mid) && (j <= right))
  {
    if (a[i] <= a[j])
    {
      b[k++] = a[i++];
    }
    else
    {
      b[k++] = a[j++];
 
     /*this is tricky -- see above explanation/diagram for merge()*/
      inv_count = inv_count + (mid - i);
    }
  }
                  //                       std::cout << "ping2" << std::endl;
  /* Copy the remaining elements of left subarray
   (if there are any) to temp*/
  while (i < mid)
    b[k++] = a[i++];
 
  /* Copy the remaining elements of right subarray
   (if there are any) to temp*/
  while (j <= right)
    b[k++] = a[j++];
 
  /*Copy back the merged elements to original array*/
  for (i=left; i <= right; i++)
    a[i] = b[i];
 

//  std::cout << "int" << inv_count << std::endl;
  return inv_count;	
                //                                                            std::cout << "ping3" << std::endl;



}


unsigned long long get_number_of_inversions(vector<int> &a, vector<int> &b, size_t left, size_t right) {
  unsigned long long number_of_inversions = 0;
  if (right <= left) return number_of_inversions;
  size_t mid = left + (right - left) / 2;
  number_of_inversions = get_number_of_inversions(a, b, left, mid);
  number_of_inversions += get_number_of_inversions(a, b, mid+1, right);
  number_of_inversions += merge(a, b, left, mid+1, right);
  return number_of_inversions;
}


int main() {
  int n;
  std::cin >> n;
  vector<int> a(n);
  for (size_t i = 0; i < a.size(); i++) {
    std::cin >> a[i];
  }
  vector<int> b(a.size());
  std::cout << get_number_of_inversions(a, b, 0, a.size()-1) << '\n';
}
