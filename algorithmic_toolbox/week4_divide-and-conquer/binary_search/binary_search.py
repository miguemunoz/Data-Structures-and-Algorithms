# Uses python3
import sys

def binary_search(a, x): 
    while len(a) > 0:
        mid = len(a)//2
        if a[mid] == x:
            return mid
        else:
            if (a[mid] < x):
                return binary_search(a[mid+1:],x)
            else:
                return binary_search(a[:mid],x)
    
    return -1

def linear_search(a, x):
    for i in range(len(a)):
        if a[i] == x:
            return i
    return -1

if __name__ == '__main__':
    input = sys.stdin.read()
    data = list(map(int, input.split()))
    n = data[0]
    m = data[n + 1]
    a = data[1 : n + 1]
    for x in data[n + 2:]:
        print(binary_search(a, x), end = ' ')
