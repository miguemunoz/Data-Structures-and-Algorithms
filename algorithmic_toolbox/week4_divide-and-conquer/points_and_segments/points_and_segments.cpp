#include <algorithm>
#include <iostream>
#include <vector>
#include <tuple>

using std::pair;
using std::vector;                            
using std::tuple;

vector<long long> fast_count_segments(vector<tuple<long long, char, int> >& vec, const vector<long long>& points) {
    long long in = 0;
    vector<long long> cnt(points.size());
    std::sort(vec.begin(), vec.end());
    for (size_t j = 0; j < vec.size(); j++)
    {
        if (std::get<1>(vec[j]) == 'l') {
            in++;
        }
        else if (std::get<1>(vec[j]) == 'r') {
            in--;
        }
        else {
            cnt[std::get<2>(vec[j])] = in;
        }
    }
    return cnt;
}

vector<long long> naive_count_segments(vector<long long> starts, vector<long long> ends, vector<long long> points) {
    vector<long long> cnt(points.size());
    for (size_t i = 0; i < points.size(); i++) {
        for (size_t j = 0; j < starts.size(); j++) {
            cnt[i] += starts[j] <= points[i] && points[i] <= ends[j];
        }
    }
    return cnt;
}

int main() {
    long long n, m;
    std::cin >> n >> m;
    vector<long long> starts(n), ends(n);
    vector<tuple<long long, char, int> > vec;
    for (size_t i = 0; i < starts.size(); i++) {
        std::cin >> starts[i] >> ends[i];
        vec.push_back(std::make_tuple(starts[i], 'l', i));
        vec.push_back(std::make_tuple(ends[i], 'r', i));
    }
    vector<long long> points(m);
    for (size_t i = 0; i < points.size(); i++) {
        std::cin >> points[i];
        vec.push_back(std::make_tuple(points[i], 'p', i));
    }
    //use fast_count_segments
    vector<long long> cnt = fast_count_segments(vec, points);
    for (size_t i = 0; i < cnt.size(); i++) {
        std::cout << cnt[i] << ' ';
    }
}
