import sys

def fibonacci_sum_naive(n):
    if n <= 1:
        return n

    previous = 0
    current  = 1
    sum      = 1

    for _ in range(n - 1):
        previous, current = current, previous + current
        sum += current

    return sum % 10

def get_fibonacci_last_digit_mod_m_fast(n,m):
    a, b = 0, 1
    for _ in range(n):
        next = (a + b) % m
        a = b
        b = next
    return a

def get_pisano_period(m):
    a = 0
    b = 1
    c = a + b
    for i in range(m*m):
        c = (a + b) % m
        a = b
        b = c
        if (a == 0 and b == 1):
            return i+1

def get_fibonacci_huge_fast(n, m):
    if n <= 1:
        return n
    remainder = n % get_pisano_period(m)
    return get_fibonacci_last_digit_mod_m_fast(remainder, m)

def fibonacci_sum_fast(n):
    return (get_fibonacci_huge_fast(n+2,10)+9) % 10

if __name__ == '__main__':
    input = sys.stdin.read()
    n = int(input)
    print(fibonacci_sum_fast(n))
