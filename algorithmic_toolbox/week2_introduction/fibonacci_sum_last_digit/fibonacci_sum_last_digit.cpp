#include <iostream>
#include <cassert>

int fibonacci_sum_naive(long long n) {
    if (n <= 1)
        return n;

    long long previous = 0;
    long long current  = 1;
    long long sum      = 1;

    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
        sum += current;
    }

    return sum % 10;
}

long long get_fibonacci_last_digit_mod_m_fast(long long n, long long m) {
    if (n > 1) {
        long long map[n+1] = {0};
        map[0] = 0;
        map[1] = 1;
        for (int i = 2; i <= n; i++)
            map[i] = (map[i-1] + map[i-2]) % m;
        return map[n];
    }
    return n;
}

long long get_pisano_period(long long m) {
    long long a = 0, b = 1, c = a + b;
    for (int i = 0; i < m * m; i++) {
        c = (a + b) % m;
        a = b;
        b = c;
        if (a == 0 && b == 1) return i + 1;
    }
}

long long get_fibonacci_huge_fast(long long n, long long m) {
    if (n <= 1)
        return n;
    long long remainder = n % get_pisano_period(m);
    return get_fibonacci_last_digit_mod_m_fast(remainder, m);
}

int fibonacci_sum_fast(long long n) {
    return (get_fibonacci_huge_fast(n+2,10)+9) % 10;
}

void test_solution() {
    assert(fibonacci_sum_fast(3) == 4);
    assert(fibonacci_sum_fast(100) == 5);
    for (int n = 1; n < 20; ++n)
        assert(fibonacci_sum_fast(n) == fibonacci_sum_naive(n));
}

int main() {
    long long n = 0;
    std::cin >> n;
    std::cout << fibonacci_sum_fast(n);
}
