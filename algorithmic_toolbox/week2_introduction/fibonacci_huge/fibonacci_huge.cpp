#include <iostream>
#include <cassert>

long long get_fibonacci_huge_naive(long long n, long long m) {
    if (n <= 1)
        return n;
    
    long long previous = 0;
    long long current  = 1;
    
    for (long long i = 0; i < n - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }
    
    return current % m;
}

long long get_fibonacci_last_digit_mod_m_fast(long long n, long long m) {
    if (n > 1) {
        long long map[n+1] = {0};
        map[0] = 0;
        map[1] = 1;
        for (int i = 2; i <= n; i++)
            map[i] = (map[i-1] + map[i-2]) % m;
        return map[n];
    }
    return n;
}

long long get_pisano_period(long long m) {
    long long a = 0, b = 1, c = a + b;
    for (int i = 0; i < m * m; i++) {
        c = (a + b) % m;
        a = b;
        b = c;
        if (a == 0 && b == 1) return i + 1;
    }
}

long long get_fibonacci_huge_fast(long long n, long long m) {
    if (n <= 1)
        return n;
    long long remainder = n % get_pisano_period(m);
    return get_fibonacci_last_digit_mod_m_fast(remainder, m);
}

void test_solution()
{
    assert(get_fibonacci_huge_fast(1,239) == 1);
    assert(get_fibonacci_huge_fast(239,1000) == 161);
    assert(get_fibonacci_huge_fast(2816213588,30524) == 10249);
    for (int i = 1; i < 20; ++i)
        for (int j = 1; j < 20; ++j)
            assert(get_fibonacci_huge_fast(i,j) == get_fibonacci_huge_naive(i,j));
}

int main() {
    long long n, m;
    std::cin >> n >> m;
    std::cout << get_fibonacci_huge_fast(n, m) << '\n';
}
