import java.util.*;

public class FibonacciHuge {
    private static long getFibonacciHugeNaive(long n, long m) {
        if (n <= 1)
            return n;
        
        long previous = 0;
        long current  = 1;
        
        for (long i = 0; i < n - 1; ++i) {
            long tmp_previous = previous;
            previous = current;
            current = tmp_previous + current;
        }
        
        return current % m;
    }

    private static long getFibonacciLastDigitModmFast(long n, long m) {
        if (n > 1) {
            long map[] = new long[(int)n+1];
            map[0] = 0;
            map[1] = 1;
            for (int i = 2; i <= n; i++)
                map[i] = (map[i-1] + map[i-2]) % m;
            return map[(int)n];
        }
        return n;
    }

    private static long getPisanoPeriod(long m) {
        long a = 0, b = 1, c = a + b;
        for (int i = 0; i < m * m; i++) {
            c = (a + b) % m;
            a = b;
            b = c;
            if (a == 0 && b == 1) return i + 1;
        }
        return m;
    }

    private static long getFibonacciHugeFast(long n, long m) {
        if (n <= 1)
            return n;
        long remainder = n % getPisanoPeriod(m);
        return getFibonacciLastDigitModmFast(remainder, m);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        long m = scanner.nextLong();
        System.out.println(getFibonacciHugeFast(n, m));
    }
}

