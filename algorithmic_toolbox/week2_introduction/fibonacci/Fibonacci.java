import java.util.Scanner;

public class Fibonacci {
    private static long calc_fib_fast(int n) {
        if (n > 1) {
            int map[] = new int[n+1];
            map[0] = 0;
            map[1] = 1;
            for (int i = 2; i <= n; i++)
                map[i] = map[i-1] + map[i-2];
            return map[n];
        }
        return n;
    }
    
    private static long calc_fib(int n) {
        if (n <= 1)
            return n;
        return calc_fib(n - 1) + calc_fib(n - 2);
    }
    
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        System.out.println(calc_fib_fast(n));
    }
}
