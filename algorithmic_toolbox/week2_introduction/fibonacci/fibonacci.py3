def calc_fib(n):
    if (n <= 1):
        return n
    return calc_fib(n-1) + calc_fib(n-2)

def calc_fib_fast(n):
    a, b = 0, 1
    for _ in range(n):
        next = a + b
        a = b
        b = next
    return a

n = int(input())
print(calc_fib_fast(n))
