#include <iostream>
#include <cassert>

long long lcm_naive(int a, int b) {
    for (long l = 1; l <= (long long) a * b; ++l)
        if (l % a == 0 && l % b == 0)
            return l;
    return (long long) a * b;
}

long long gcd_fast(long long a, long long b) {
    if (b == 0)
        return a; 
    else
        return gcd_fast(b, a % b);
}

long long lcm_fast(long long a, long long b) {
    return (a*b)/gcd_fast(a,b);
}

void test_solution() {
    assert(lcm_fast(6,8) == 24);
    assert(lcm_fast(28851538,1183019) == 1933053046);
    for (int i = 1; i < 5; i++)
        for (int j = 1; i < 5; j++)
            assert(lcm_fast(i,j) == lcm_naive(i,j));
}

int main() {
    int a, b;
    std::cin >> a >> b;
    std::cout << lcm_fast(a, b) << std::endl;
    return 0;
}
