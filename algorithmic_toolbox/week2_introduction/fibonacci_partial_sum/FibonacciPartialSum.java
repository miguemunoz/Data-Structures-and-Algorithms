import java.util.*;

public class FibonacciPartialSum {
    private static long getFibonacciPartialSumNaive(long from, long to) {
        if (to <= 1)
            return to;

        long previous = 0;
        long current  = 1;

        for (long i = 0; i < from - 1; ++i) {
            long tmp_previous = previous;
            previous = current;
            current = tmp_previous + current;
        }

        long sum = current;

        for (long i = 0; i < to - from; ++i) {
            long tmp_previous = previous;
            previous = current;
            current = tmp_previous + current;
            sum += current;
        }

        return sum % 10;
    }
    
    private static long getFibonacciLastDigitModmFast(long n, long m) {
        if (n > 1) {
            long map[] = new long[(int)n+1];
            map[0] = 0;
            map[1] = 1;
            for (int i = 2; i <= n; i++)
                map[i] = (map[i-1] + map[i-2]) % m;
            return map[(int)n];
        }
        return n;
    }
    
    private static long getPisanoPeriod(long m) {
        long a = 0, b = 1, c = a + b;
        for (int i = 0; i < m * m; i++) {
            c = (a + b) % m;
            a = b;
            b = c;
            if (a == 0 && b == 1) return i + 1;
        }
        return m;
    }
    
    private static long getFibonacciHugeFast(long n, long m) {
        if (n <= 1)
            return n;
        long remainder = n % getPisanoPeriod(m);
        return getFibonacciLastDigitModmFast(remainder, m);
    }
    
    private static long getFibonacciSumFast(long n) {
        return (getFibonacciHugeFast(n+2,10)+9) % 10;
    }
    
    private static long getFibonacciPartialSumFast(long from, long to) {
        if (to <= 1)
            return to;
        
        long res_to = getFibonacciSumFast(to);
        long res_from = getFibonacciSumFast(from-1);
        long res = res_to-res_from;
        
        if (res >= 0)
            return res;
        else
            return (res_to + 10) - res_from;
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long from = scanner.nextLong();
        long to = scanner.nextLong();
        System.out.println(getFibonacciPartialSumFast(from, to));
    }
}

