#include <iostream>
#include <cassert>

long long get_fibonacci_partial_sum_naive(long long from, long long to) {
    if (to <= 1)
        return to;

    long long previous = 0;
    long long current  = 1;

    for (long long i = 0; i < from - 1; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
    }

    long long sum = current;

    for (long long i = 0; i < to - from; ++i) {
        long long tmp_previous = previous;
        previous = current;
        current = tmp_previous + current;
        sum += current;
    }

    return sum % 10;
}

long long get_fibonacci_last_digit_mod_m_fast(long long n, long long m) {
    if (n > 1) {
        long long map[n+1] = {0};
        map[0] = 0;
        map[1] = 1;
        for (int i = 2; i <= n; i++)
            map[i] = (map[i-1] + map[i-2]) % m;
        return map[n];
    }
    return n;
}

long long get_pisano_period(long long m) {
    long long a = 0, b = 1, c = a + b;
    for (int i = 0; i < m * m; i++) {
        c = (a + b) % m;
        a = b;
        b = c;
        if (a == 0 && b == 1) return i + 1;
    }
}

long long get_fibonacci_huge_fast(long long n, long long m) {
    if (n <= 1)
        return n;
    long long remainder = n % get_pisano_period(m);
    return get_fibonacci_last_digit_mod_m_fast(remainder, m);
}

int fibonacci_sum_fast(long long n) {
    return (get_fibonacci_huge_fast(n+2,10)+9) % 10;
}

long long get_fibonacci_partial_sum_fast(long long from, long long to) {
    if (to <= 1)
        return to;
    
    long long res_to = fibonacci_sum_fast(to);
    long long res_from = fibonacci_sum_fast(from-1);
    long long res = res_to-res_from;
    
    if (res >= 0)
        return res;
    else
        return (res_to + 10) - res_from;
}

void test_solution() {
    assert(get_fibonacci_partial_sum_fast(3,7) == 1);
    assert(get_fibonacci_partial_sum_fast(10,10) == 5);
    assert(get_fibonacci_partial_sum_fast(10,200) == 2);
   for (int i = 1; i < 20; ++i)
    for (int j = 1; j < 20; ++j)
        assert(get_fibonacci_partial_sum_fast(i,j) == get_fibonacci_partial_sum_naive(i,j));
}

int main() {
    long long from, to;
    std::cin >> from >> to;
    std::cout << get_fibonacci_partial_sum_fast(from, to) << '\n';
}
