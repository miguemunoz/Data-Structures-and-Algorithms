import sys

def fibonacci_partial_sum_naive(from_, to):
    sum = 0

    current = 0
    next  = 1

    for i in range(to + 1):
        if i >= from_:
            sum += current

        current, next = next, current + next

    return sum % 10

def fibonacci_sum_naive(n):
    if n <= 1:
        return n

    previous = 0
    current  = 1
    sum      = 1

    for _ in range(n - 1):
        previous, current = current, previous + current
        sum += current

    return sum % 10

def get_fibonacci_last_digit_mod_m_fast(n,m):
    a, b = 0, 1
    for _ in range(n):
        next = (a + b) % m
        a = b
        b = next
    return a

def get_pisano_period(m):
    a = 0
    b = 1
    c = a + b
    for i in range(m*m):
        c = (a + b) % m
        a = b
        b = c
        if (a == 0 and b == 1):
            return i+1

def get_fibonacci_huge_fast(n, m):
    if n <= 1:
        return n
    remainder = n % get_pisano_period(m)
    return get_fibonacci_last_digit_mod_m_fast(remainder, m)

def fibonacci_sum_fast(n):
    return (get_fibonacci_huge_fast(n+2,10)+9) % 10

def get_fibonacci_partial_sum_fast(from_, to):
    if (to <= 1):
        return to
    
    res_to = fibonacci_sum_fast(to)
    res_from = fibonacci_sum_fast(from_-1)
    res = res_to-res_from
    
    if (res >= 0):
        return res
    else:
        return (res_to + 10) - res_from

if __name__ == '__main__':
    input = sys.stdin.read()
    from_, to = map(int, input.split())
    print(get_fibonacci_partial_sum_fast(from_, to))