import sys

def get_fibonacci_last_digit_fast(n):
    a, b = 0, 1
    for _ in range(n):
        next = (a + b) % 10
        a = b
        b = next
    return a

def get_fibonacci_last_digit_naive(n):
    if n <= 1:
        return n
    
    previous = 0
    current  = 1
    
    for _ in range(n - 1):
        previous, current = current, previous + current
    
    return current % 10

if __name__ == '__main__':
    input = sys.stdin.read()
    n = int(input)
    print(get_fibonacci_last_digit_fast(n))
