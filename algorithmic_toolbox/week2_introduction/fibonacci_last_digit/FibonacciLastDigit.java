import java.util.*;

public class FibonacciLastDigit {
    private static int getFibonacciLastDigitFast(int n) {
        if (n > 1) {
            int map[] = new int[n+1];
            map[0] = 0;
            map[1] = 1;
            for (int i = 2; i <= n; i++)
                map[i] = (map[i-1] + map[i-2]) % 10;
            return map[n];
        }
        return n;
    }
    
    private static int getFibonacciLastDigitNaive(int n) {
        if (n <= 1)
            return n;
        
        int previous = 0;
        int current  = 1;
        
        for (int i = 0; i < n - 1; ++i) {
            int tmp_previous = previous;
            previous = current;
            current = tmp_previous + current;
        }
        
        return current % 10;
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int c = getFibonacciLastDigitFast(n);
        System.out.println(c);
    }
}
